exiftool \
-Title="Global monitoring platform for coastal zones" \
-Author="Cristina Lira, Ksenia Nazirova, Renato Mendes, Marius Budileanu, Ioan Serban, David Blondeau, Razvan Mateescu and Edward P. Morris" \
-Subject="Student team-work presentation given at the 5th ESA Advanced Training on Ocean Remote Sensing and Synergy, 2017-09-15" \
-Keywords="ESA" \
-Keywords="Copernicus" \
-Keywords="Coast" \
-Keywords="Platform" \
-Keywords="Earth Observation" \
global_monitoring_platform_for_coastal_zones_ESA_OTC2017.pdf
